package com.biom4st3r.moenchantments.Items;

import net.minecraft.item.Item;

public class DummyHeroProjectileItem extends Item
{
    public DummyHeroProjectileItem()
    {
        super(new Settings().stackSize(1));
    }

}