package com.biom4st3r.moenchantments.mixin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;
import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.Logic.onBreakMixinLogic;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.BasicInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.loot.LootSupplier;
import net.minecraft.world.loot.LootTables;
import net.minecraft.world.loot.context.LootContext;
import net.minecraft.world.loot.context.LootContextParameters;
import net.minecraft.world.loot.context.LootContextTypes;

@Mixin(Block.class)
public class BlockMixin
{
    @Inject(at = @At("HEAD"),method = "onBreak",cancellable = false)
    public void afterBreak(World world, BlockPos blockPos, BlockState blockState, PlayerEntity pe,CallbackInfo ci)
    {
        onBreakMixinLogic.tryVeinMining(world, blockPos, blockState, pe);   
    }

    @Inject(at = @At("HEAD"),method = "getDroppedStacks", cancellable = true)
    public void getDroppedStacks(BlockState blockState, net.minecraft.world.loot.context.LootContext.Builder lootBuilder, CallbackInfoReturnable<List<ItemStack>> ci)
    {

        ItemStack tool = lootBuilder.get(LootContextParameters.TOOL);
        Identifier loottableId = ((Block)(Object)this).getDropTableId();
        if(EnchantmentHelper.getLevel(MoEnchants.AUTOSMELT, tool) > 0 && loottableId != LootTables.EMPTY)
        {
            Entity entity = lootBuilder.get(LootContextParameters.THIS_ENTITY);
            lootBuilder.put(LootContextParameters.BLOCK_STATE, blockState);
            LootContext lootContext = lootBuilder.build(LootContextTypes.BLOCK);
            ServerWorld serverworld = lootContext.getWorld();
            LootSupplier lootSuppier = serverworld.getServer().getLootManager().getSupplier(loottableId);
            List<ItemStack> stacks = lootSuppier.getDrops(lootContext);
            // System.out.println(stacks.get(0).getAmount());
            // System.out.println(stacks.get(1).getAmount());
            // System.out.println(stacks.size() + "\n");
            Optional<SmeltingRecipe> smeltingResult;// = entity.world.getRecipeManager()
            // .getFirstMatch(RecipeType.SMELTING,
            //         new BasicInventory((stacks.size() > 0 ? stacks.get(0) : ItemStack.EMPTY)), entity.world);
            RecipeManager rm = entity.world.getRecipeManager();
            Inventory basicInv = new BasicInventory();
            ItemStack temp = ItemStack.EMPTY;
            for(int i = 0; i < stacks.size(); i++)
            {
                //System.out.println(i);
                temp = stacks.get(i);
                basicInv = new BasicInventory(temp);
                smeltingResult = rm.getFirstMatch(RecipeType.SMELTING, basicInv, entity.world);
                if(smeltingResult.isPresent() && !(MoEnchantmentsMod.autoSmelt_blacklist.contains(temp.getItem())))
                {
                    stacks.set(i, new ItemStack(smeltingResult.get().getOutput().getItem(),temp.getAmount()));
                    tool.getTag().putFloat("smeltexperience", 
                        (smeltingResult.get().getExperience()*temp.getAmount()) + 
                        (tool.getTag().containsKey("smeltexperience") ? tool.getTag().getFloat("smeltexperience") : 0));
                    serverworld.playSound((PlayerEntity)null, entity.getBlockPos(), SoundEvents.BLOCK_LAVA_EXTINGUISH, SoundCategory.BLOCKS, 0.5f, 0.5f);
                
                }
            }
            ci.setReturnValue(stacks);
            // if(smeltingResult.isPresent() && !(MoEnchantmentsMod.autoSmelt_blacklist.contains(stacks.get(0).getItem())) && stacks.get(0).getItem() != Items.POTATO)
            // {
            //     tool.getTag().putFloat("smeltexperience", 
            //         smeltingResult.get().getExperience() + 
            //         (tool.getTag().containsKey("smeltexperience") ? tool.getTag().getFloat("smeltexperience") : 0));
            //     List<ItemStack> stacksSmelted = new ArrayList<ItemStack>();//stacks.set(0, smeltingResult.get().getOutput());
            //     stacksSmelted.add(smeltingResult.get().getOutput().copy());
            //     stacksSmelted.get(0).setAmount(stacks.get(0).getAmount());
            //     ci.setReturnValue(stacksSmelted);
            // }

        }

    }
}