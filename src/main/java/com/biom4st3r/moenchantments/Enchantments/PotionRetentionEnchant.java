package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;

public class PotionRetentionEnchant extends MoEnchant {

    public PotionRetentionEnchant()
    {
        super(Weight.UNCOMMON, EnchantmentTarget.WEAPON, new EquipmentSlot[]{EquipmentSlot.MAINHAND});
    }

    @Override
    public String getDisplayName() {
        return "Imbued";
    }

    @Override
    public String regName() {
        return "potionretension";
        
    }

    @Override
    public int getMinimumPower(int int_1) {
        return int_1 * 7;
    }

    @Override
    public int getMaximumLevel() {
        return MoEnchantmentsMod.config.PotionRetentionMaxLevel;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        Item i = iS.getItem();
        return i instanceof SwordItem || i instanceof AxeItem;// || i instanceof BowItem;
    }

    @Override
    public boolean enabled() {
        return MoEnchantmentsMod.config.EnablePotionRetention;
    }
    
}