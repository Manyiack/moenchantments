package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import net.minecraft.enchantment.EnchantmentTarget;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;

public class EnderProtectionEnchant extends MoEnchant 
{

    public EnderProtectionEnchant()
    {
        super(Weight.VERY_RARE, EnchantmentTarget.ARMOR, new EquipmentSlot[] { EquipmentSlot.CHEST, EquipmentSlot.FEET,
                EquipmentSlot.LEGS, EquipmentSlot.HEAD });
    }

    @Override
    public String getDisplayName() {
        return "Curse of The End";//erman";
        
    }

    @Override
    public int getMaximumLevel() {
        //3 teleports within 8 blocks of attacker
        //2 1 + looks at attacker
        //1 2 + chance to prevent damage
        return 3;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }
    @Override
    public int getMinimumPower(int int_1) {
        return int_1*10;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        return this.type.isAcceptableItem(iS.getItem());
    }
    

    @Override
    public String regName() {
        return "curseofender";
    }
    @Override
    public boolean isTreasure() {
        return true;
    }
    @Override
    public boolean isCursed() {
        return true;
    }
    @Override
    public boolean enabled()
    {
        return MoEnchantmentsMod.config.EnableEnderProtection;
    }
    
}