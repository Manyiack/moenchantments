package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;

public class HerosSwordEnchant extends MoEnchant {

    public HerosSwordEnchant()
    {
        super(Weight.RARE, EnchantmentTarget.WEAPON, new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    @Override
    public String getDisplayName() 
    {
        return "Hero's Sword";
    }

    @Override
    public String regName() 
    {
        return "herossword";
    }

    @Override
    public int getMinimumPower(int int_1) 
    {
        return 40;
    }

    @Override
    public int getMaximumLevel() 
    {
        return 1;
    }

    @Override
    public int getMinimumLevel() 
    {
        return 1;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) 
    {
        return this.type.isAcceptableItem(iS.getItem());
    }

    @Override
    public boolean enabled() {
        return false;//MoEnchantmentsMod.config.enable;
    }

}